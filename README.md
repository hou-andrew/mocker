Sanely generate the infrastructure needed for developing a multi-component service in Docker.

Generates the following tree:

```
<app root>
├── Dockerfile.base
├── Dockerfile.development # You'll install testing libraries and such in here.
├── Dockerfile.production # You may not have anything to put in here.
├── Makefile
├── Makefile.internal
├── bin
│   ├── entrypoint.erb.sh
│   └── make # In the Dockerfile, symlinked to /usr/local/bin/make
└── docker-compose.yml
```

# Installation

* ruby > 2.1 is required.
* `gem install mocker`

## Make Targets

* `make` - initialize everything, shortcut for
    - `pull`
    - `images`
    - `start_resources`
    - `setup`
    - `build`
* `make pull` - Pull all image dependencies
* `make images` - Build all images that need building
* `make setup` - run any setup or seeding that is needed.
* `make up` - start application containers
* `make down` - stop application containers
* `make implode` - `docker-compose down --rmi all --volumes`
* `make shell` - Run a shell container
* `make production` - Build the non-development image
* `make build` - An example for how to pass-through to the `Makefile.internal`

## Development Workflow

### Initial

```shell
make && make up
```

### Code Iterations

1. Make your code changes
2. If using a compiled language, `make build` (or any pass-through target)
3. `make run`, or `make up` depending on how you wish to see output.

### Production builds

`make production`

## Notes & Explainations

* The main thing going on here is that your main services in the `docker-compose.yml` file have a bind-mounted source directory.
* Once you run this project, all the generated files constitue the whole thing.  After initializing the project with `mocker init`, you do not need `mocker` any longer.  Nor do any team members ever need to install it.  You are free to modify and change anything that you want.
