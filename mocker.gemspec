lib_path  = "./lib"
spec_path = "./spec"
bin_path  = "./bin"

libs  = Dir.glob("#{lib_path}/**/*.rb")
specs = Dir.glob("#{spec_path}/**/*.rb")
bins  = Dir.glob("#{bin_path}/**/*")

flist = libs + specs + bins
flist << "asa-common.gemspec"
flist << "README.md"

require_relative './lib/version'

summary = <<-EOD
A config generation library to generate a few Dockerfiles, Makefile, and
docker-compose.yml file that will get everything setup for local docker
development.  Specifically meant for use with non-trivial development setups.
EOD

Gem::Specification.new do |gem|
  gem.name          = "mocker"
  gem.version       = Mocker::VERSION
  gem.authors       = ["Brett Nakashima"]
  gem.email         = ["brettnak@gmail.com"]
  gem.description   = "Docker development project generator"
  gem.summary       = summary
  gem.homepage      = "http://gitlab.com/brettnak/mocker"

  gem.files         = flist
  gem.executables   = bins.map{ |f| File.basename(f) }
  gem.test_files    = specs
  gem.require_paths = ["lib/"]

  gem.add_development_dependency 'rspec', '~> 3.1'
  gem.add_development_dependency 'trollop', '~> 2.1'

  gem.license = "MIT"
end
